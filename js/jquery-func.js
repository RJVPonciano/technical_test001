$( document ).ready(function() {
    document.getElementById("tot").value = 0;						// inicializar valor de carrito en 0
	
	document.getElementById("prodA").style.visibility = "hidden";	// ocultar productos de lista de productos
	document.getElementById("prodB").style.visibility = "hidden";
	document.getElementById("prodC").style.visibility = "hidden";
	document.getElementById("prodD").style.visibility = "hidden";
	document.getElementById("prodE").style.visibility = "hidden";
});
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
function agregarA(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) + 250;										// Añadir precio a variable del carrito

	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantA").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) + 1;											// Añadir uno a la cantidad
	document.getElementById("cantA").innerHTML = v2;				// Añadir nueva cantidad	
	
	document.getElementById("prodA").style.visibility = "visible"; 	// Hacer visible el producto A en lista de productos
};
// ---------------------------------------------------------------------------------------------------------
function agregarB(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) + 350;										// Añadir precio a variable del carrito
		
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantB").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) + 1;											// Añadir uno a la cantidad
	document.getElementById("cantB").innerHTML = v2;				// Añadir nueva cantidad	
	
	document.getElementById("prodB").style.visibility = "visible"; 	// Hacer visible el producto A en lista de productos
};
// ---------------------------------------------------------------------------------------------------------
function agregarC(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) + 200;										// Añadir precio a variable del carrito
		
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantC").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) + 1;											// Añadir uno a la cantidad
	document.getElementById("cantC").innerHTML = v2;				// Añadir nueva cantidad	
	
	document.getElementById("prodC").style.visibility = "visible"; 	// Hacer visible el producto A en lista de productos
};
// ---------------------------------------------------------------------------------------------------------
function agregarD(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) + 500;										// Añadir precio a variable del carrito
		
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantD").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) + 1;											// Añadir uno a la cantidad
	document.getElementById("cantD").innerHTML = v2;				// Añadir nueva cantidad	
	
	document.getElementById("prodD").style.visibility = "visible"; 	// Hacer visible el producto A en lista de productos
};
// ---------------------------------------------------------------------------------------------------------
function agregarE(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) + 100;										// Añadir precio a variable del carrito
		
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantE").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) + 1;											// Añadir uno a la cantidad
	document.getElementById("cantE").innerHTML = v2;				// Añadir nueva cantidad	
	
	document.getElementById("prodE").style.visibility = "visible"; 	// Hacer visible el producto A en lista de productos
};
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
function QuitarA(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) - 250;										// Restar precio a variable del carrito
	
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantA").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) - 1;											// Restar uno a la cantidad
	document.getElementById("cantA").innerHTML = v2;				// Añadir nueva cantidad	
	
	if(v2 <= 0){
		document.getElementById("prodA").style.visibility = "hidden";	// Ocultar el producto cuando la cantidad llegue a 0
	}
};
// ---------------------------------------------------------------------------------------------------------
function QuitarB(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) - 350;										// Restar precio a variable del carrito
	
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantB").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) - 1;											// Restar uno a la cantidad
	document.getElementById("cantB").innerHTML = v2;				// Añadir nueva cantidad	
	
	if(v2 <= 0){
		document.getElementById("prodB").style.visibility = "hidden";	// Ocultar el producto cuando la cantidad llegue a 0
	}
};
// ---------------------------------------------------------------------------------------------------------
function QuitarC(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) - 200;										// Restar precio a variable del carrito
	
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantC").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) - 1;											// Restar uno a la cantidad
	document.getElementById("cantC").innerHTML = v2;				// Añadir nueva cantidad	
	
	if(v2 <= 0){
		document.getElementById("prodC").style.visibility = "hidden";	// Ocultar el producto cuando la cantidad llegue a 0
	}
};
// ---------------------------------------------------------------------------------------------------------
function QuitarD(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) - 500;										// Restar precio a variable del carrito
	
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantD").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) - 1;											// Restar uno a la cantidad
	document.getElementById("cantD").innerHTML = v2;				// Añadir nueva cantidad	
	
	if(v2 <= 0){
		document.getElementById("prodD").style.visibility = "hidden";	// Ocultar el producto cuando la cantidad llegue a 0
	}
};
// ---------------------------------------------------------------------------------------------------------
function QuitarE(){
	v1 = document.getElementById("tot").value; 						// Extraer valor de carrito
	v1 = parseInt(v1) - 100;										// Restar precio a variable del carrito
	
	document.getElementById("tot").value = v1;						// Añadir precio a carrito
	document.getElementById("tot2").innerHTML = "$"+v1;
	
	v2 = document.getElementById("cantE").innerHTML;				// Extraer valor de cantidad de producto
	v2 = parseInt(v2) - 1;											// Restar uno a la cantidad
	document.getElementById("cantE").innerHTML = v2;				// Añadir nueva cantidad	
	
	if(v2 <= 0){
		document.getElementById("prodE").style.visibility = "hidden";	// Ocultar el producto cuando la cantidad llegue a 0
	}
};