# technical_test001

Simulador del carrito de compras.

Descripción.

	-El programa está dividido en 3 secciones:
		*Parte superior: Simulación del carrito de compras, se añaden o quitan productos.
		*Parte central: Total de las compras efectuadas. 
		*Parte inferior: Catálogo de productos.

Funcionalidad.

	*Parte inferior (Catálogo de productos).
		* El catálogo de productos consta de 5 productos ( A,B,C,D,E ), cada uno con su respectivo precio.
		* Cada producto contiene 3 etiquetas (Nombre, Precio, Agregar al carrito) 
		* El botón de agregar al carrito contiene la funcionalidad de llamar a un jquery, que realiza la acción de agregar visualmente al carrito el producto seleccionado.
			- En caso de selecionar repetidamente un mismo producto, en el carrito se agregó la etiqueta de cantidad que llevará el conteo de los productos selecionados.

	*Parte Superior (carrito de compras).
		* EL carrito contendrá todos los productos selecionados ( A,B,C,D,E ), cada uno con su respectiva cantidad seleccionada.
		* Cada producto contiene un botón de "remover", que retirará un producto.
			- En caso de que la cantidad sea mayor a 1, el botón de "remover" lo irá reduciendo de uno en uno.
			- Cuando el producto llegue a "0" en la cantidad, el producto desaparecerá del carrito.
	
	*Parte Central (Total).
		* El total está automatizado.
			- Al agregar un producto se va sumando el precio del producto a la cantidad total.
			- Al remover un producto se va restando el precio del producto a la cantidad total.
			- En caso de remover todos los productos el carrito volverá a "0"
			- En caso de refrescar la página ("F5") se borrarán los productos seleccionados y el carrito volvera a "0". 

Correr Aplicación.
	
	* Abrir el archivo "Index.html" (ruta: technical_test001/index.html).


Estructura de archivos:

	* Archivo "Index.html" (ruta: technical_test001/index.html)
		- Contiene toda la estructura de la página.

	* Archivo "style.css" (ruta: technical_test001/css/style.css)
		- Contiene todo el formato de la página  

	* Archivo "jquery-func.js" (ruta: technical_test001/js/jquery-func.js)
		- Contiene la funcionalidad de la página.

	* Imágenes (ruta: technical_test001/css/images/imagen.jpg).


Anexo:

	* La página no contiene Base de Datos, esto para que no dependa de instalaciones previas y pueda correr en cualquier computadora.
		- Esto limita al programa a que el catálogo esté sobre código y no sea dinámico.


